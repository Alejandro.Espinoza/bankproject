package bankproject;


import java.util.Date;

public class FixedTermAccount extends Account {
    private Date fixedTermDate ;

    public void setFixedData(Client client, Date date, double money, Date endDate) {
        this.fixedTermDate = endDate;
        this.creationDate = date;
        this.ownerName = client.getClientName();
        this.passId = client.getIdCard();
        this.moneyQuantity = money;
    }

    public double withDrawfix(double withDrawQuantity, String password,Date actualDate) {

        if (password == passId && actualDate == fixedTermDate) {
            moneyQuantity = moneyQuantity - withDrawQuantity;
            return moneyQuantity;
        }
        return moneyQuantity;
    }

}
