package bankproject;
public class Client {
    private String clientName;
    private String idCard;
    private String ClientOccupation;
    private String HouseAddress;
    private String emailAddress;
    private int phoneNumber;

    public void setData(String name, String occupation, String address, String email, int phone, String id) {
        this.clientName = name;
        this.ClientOccupation = occupation;
        this.HouseAddress = address;
        this.emailAddress = email;
        this.phoneNumber = phone;
        this.idCard = id;

    }

    public String getClientName() {
        return clientName;
    }

    public String getIdCard() {
        return idCard;
    }

    public String getClientOccupation() {
        return ClientOccupation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
    public String getHouseAddress() {
        return HouseAddress;
    }
    public int getPhoneNumber() {
        return phoneNumber;
    }

}
