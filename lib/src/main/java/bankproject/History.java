package bankproject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class History {

    private String transactionType;
    private String date;
    private String ownerName;
    private double quantity;
    private Date actualDate = new Date();
    private SimpleDateFormat simple= new SimpleDateFormat("dd/MM/yy HH:mm:ss"); 

    public History(String transaction, double money,String owner) {
        transactionType = transaction;
        ownerName=owner;
        quantity = money;
        date = simple.format(actualDate);
    }

    public double getQuantity() {
        return quantity;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String actualDate() {
        return simple.format(actualDate);
    }

    public ArrayList<History> historyList(ArrayList <History> history) {
        return history;
    }


}