package bankproject;

import java.util.Date;
import java.text.SimpleDateFormat;

public abstract class Account {
    protected Date creationDate = new Date();
    protected String format1 = new SimpleDateFormat("dd-MM-yyyy").format(creationDate);
    protected String ownerName;
    protected String passId;
    protected double moneyQuantity;

    public void setData(Client client, Date date, double money) {
        this.creationDate = date;
        this.ownerName = client.getClientName();
        this.passId = client.getIdCard();
        this.moneyQuantity = money;
    }
    public String getOwnerName() {
        return ownerName;
    }
    public String getPassId() {
        return passId;
    }

    public double depositMoney(double DepositQuantity, String password) {
        if (password == passId) {
            moneyQuantity = moneyQuantity + DepositQuantity;
            return moneyQuantity;
        }
        return moneyQuantity;
    }

    public double withdrawMoney(double withDrawQuantity, String password) {
        if (password == passId) {
            moneyQuantity = moneyQuantity - withDrawQuantity;
            return moneyQuantity;
        }
        return moneyQuantity;

    }

}