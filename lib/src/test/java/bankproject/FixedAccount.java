package bankproject;

import org.junit.Test;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FixedAccount {
    @Test
    public void FixedAccountCreate() {
        Date myDate = new Date();
        myDate = new Date(2021 / 05 / 12);
        Client client = new Client();
        client.setData("paquita la delBarrio", "cocinera", "av peru", "laMaspaquita@gmail.com", 76450812, "12689452");
        Account account = new FixedTermAccount();
        account.setData(client, myDate, 30000);

        assertEquals("paquita la delBarrio", account.getOwnerName());
        assertEquals("12689452", account.getPassId());
    }

    @Test
    public void wtihdrawAndDeposit() throws ParseException {
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
        Date d1 = sdformat.parse("2019-04-15");
        Date d2 = sdformat.parse("2019-08-10");
        Date d3 = sdformat.parse("2019-08-10");
        Client client = new Client();
        client.setData("paquita la delBarrio", "cocinera", "av peru", "laMaspaquita@gmail.com", 76450812, "12689452");
        FixedTermAccount account = new FixedTermAccount();
        account.setFixedData(client, d1, 30000, d2);

        double actual = account.depositMoney(5000, "12689452");
        double actual2 = account.withDrawfix(20000, "12689452", d3);

        assertEquals(35000, actual, 0.0);
        assertNotEquals(15000, actual2, 0.0);

    }

}
