package bankproject;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClientTest{
    @Test 
    public void createClientTest() {
        Client client = new Client();
        client.setData("carlos mondongo rapido", "pastelero", "calle tarija", "torta@gmail.com", 55512331, "12675867");
        assertEquals("pastelero", client.getClientOccupation());
        assertEquals("12675867", client.getIdCard());

    }
    @Test
    public void createClientsTest() {
        Client client = new Client();
        client.setData("carlos mondongo rapido", "pastelero", "calle tarija", "torta@gmail.com", 55512331, "12675867");
        assertEquals("pastelero", client.getClientOccupation());
        assertEquals("12675867", client.getIdCard());
        Client client2 = new Client();
        client2.setData("pepe kun aguero", "abogado", "calle la paz", "kunsito@gmail.com", 342341123, "12908867");
        assertEquals("abogado", client2.getClientOccupation());
        assertEquals("12908867", client2.getIdCard());
        Client client3 = new Client();
        client3.setData("pedro sanchez figo", "ing civil", "calle america", "figoSanchez@gmail.com",79832131, "17865867");
        assertEquals("ing civil", client3.getClientOccupation());
        assertEquals("17865867", client3.getIdCard());
    }

}