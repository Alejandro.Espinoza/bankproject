package bankproject;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Date;

public class SavingsAccountTest {
    @Test
    public void createAccount() {
        Date myDate = new Date();
        myDate = new Date(2021 / 04 / 3);
        Client client = new Client();
        client.setData("carlos mondongo rapido", "pastelero", "calle tarija", "torta@gmail.com", 55512331, "12675867");
        Account account = new SavingsAccount();
        account.setData(client, myDate, 6200);

        assertEquals("12675867", account.getPassId());
        assertEquals("carlos mondongo rapido", account.getOwnerName());

    }
    @Test
    public void SavingAndDepositTest() {
        Date myDate = new Date();
        myDate = new Date(2021 / 04 / 3);
        Client client = new Client();
        client.setData("carlos mondongo rapido", "pastelero", "calle tarija", "torta@gmail.com", 55512331, "12675867");
        Account account = new SavingsAccount();
        account.setData(client, myDate, 6200);
        double actual = account.depositMoney(1000, "12675867");
        double actual2 = account.withdrawMoney(1000, "12675867");

        assertEquals(7200, actual, 0.0);
        assertEquals(6200, actual2, 0.0);

    }

}