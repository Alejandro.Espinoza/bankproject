package bankproject;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Date;
public class AccountTest{
    @Test
    public void createAccount() {
        Date myDate = new Date();
        myDate = new Date(2021/04/3);
        Client client = new Client();
        client.setData("carlos mondongo rapido", "pastelero", "calle tarija", "torta@gmail.com", 55512331, "12675867");
        Account account = new SavingsAccount();
        account.setData(client,myDate, 6200);

        assertEquals("12675867", account.getPassId());
        assertEquals("carlos mondongo rapido", account.getOwnerName());

    }
}