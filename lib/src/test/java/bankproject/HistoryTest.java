package bankproject;

import org.junit.Test;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HistoryTest {
    @Test
    public void historyTest() throws ParseException {

        ArrayList<History> historylist = new ArrayList<History>();
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
        Date d1 = sdformat.parse("2019-04-15");
        Date d2 = sdformat.parse("2019-08-10");
        Client client = new Client();
        client.setData("paquita la delBarrio", "cocinera", "av peru", "laMaspaquita@gmail.com", 76450812, "12689452");
        FixedTermAccount account = new FixedTermAccount();
        account.setFixedData(client, d1, 30000, d2);
        History history = new History("fixed account", 30000, account.getOwnerName());

        assertNotEquals(null, history.historyList(historylist));

    }
}
